package com.pingan.crawler.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.transform.Result;

/**
 * 平安行内查询：企业工商信息
 *
 * @author ZhuWen
 */
@Controller
public class CourtCrawlerController extends com.pingan.crawler.controller.base.BaseController {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
            .getLogger(CourtCrawlerController.class);

    /**
     * @return
     */
    @RequestMapping(value = "/webcrawler/findValidatedCode/court", method = RequestMethod.POST)
    @ResponseBody
    public String findValidatedCodeForCrawler() {
        String json = null;
        String url = super.properties.get("url.court.findValidatedCode").toString();
        String httpResult = super.doPost(url);
        //如果是JSON
        if (httpResult.startsWith("{")) {
            json = httpResult;
        } else {
            json = "{\"status\":201,\"errorMsg\":\"" + httpResult + "\"}";
        }
        LOG.info("爬虫程序轮询行内获取验证码请求【法院执行信息（个人、企业）】 httpResult=" + json);
        return json;
    }

    /**
     * 第一次返回验证码
     */
    @RequestMapping(value = "/webcrawler/returnValidatedCode/court", method = RequestMethod.POST)
    @ResponseBody
    public String returnValidatedCodeForCrawler(
            @RequestParam final String orderNo, @RequestParam final String route) {
        String url = super.properties.get("url.court.returnValidatedCode").toString();
        String result;
        if ("官方".equals(route)) {
            String validatedCode = "iVBORw0KGgoAAAANSUhEUgAAAFoAAAAkCAYAAAAJgC2zAAALxElEQVR4nO2ZaZBdxXXHf6e7773vvZnRaBntK3ICAUmExZig2NgOBgsIxkCKEH+AiokrhcHEcYCkUsQ4jh0wia2KF8CWTZyUiV0hdooQonIcCDGhhIxAAgmkgEAaLYO20Yw027tL98mH+zQjad7gkcuefMj8P7y17+nT/+4+/T+nRVWVSfzCYf6vHfj/gkmiJwiTRE8QJomeILimv+qxNwUCCEgwBEANiAqiICgq2mgZUDGIGgxSGhFovIyY1oZ5UUzjSyGACk5L+yUERBufTrRxso+lPQFVjHq8WLwoBo8BRC2CGe73uB5G+VcaLcA71BZ4CViNAUWkSdtxoumKViAoQCBogYaCkAfUKz5AQMEDBXhVQpGjZOQhK4lUxZPTXM4oqp6MgFeP5h4NQuYVDUoeMoIqBPDl9DW14lUJxzkcfPmbBo/3SpEqPvOEvCAv8pNG11hAY9hOQz+agWYpPgxSeCWEvGnb8aLpig4SKKQgUof4iFxyBqM+nEIihkEKcGBIyInBQlVj4sIhQHBKKgVVolG2hYBhCNGEQizOKoQhvB3isAQq1LA4EBo7ozlSE3BqiCgXqdGMQgr2mJSQepym1GxE1bZj1BEIyAk7TFCarmcKY8EplgQVT92mxAximX6K9I6gKdGCYrQMEd4GNh7YxEMbHiS3Q4ga6i7DBUiKCp4aqQmsmvd+PrLsWiqaIKIY4qYdKoLXhDg4cuPZxwEeeu4BdvV1sqCygDvf9ydURYBQhiikKRtxCOVEqDBAnaPmCGu3/ivf73yUXT3dHJY+plcrXDrn17n13E+w0P0ykYJgAIPKsbGORiWrsT/u5R9f+DYb979AWzyPu9/zSeZUfjaSYSyi1eDUgEJmU7qHDrBh/3p6WvopbPlIJfdY9aRJoN/mLEo7yOyVJFhEhNhHYEaT5DHkYjBFIDNHWPPSGr6x7xGyaIALh87H4pAgqCkA2zw+A86X78FCt+nj/v/8Ej/a+e8s6JjPladfwhHfzVO7nuHRN5+gyGL+8uL7MFjcWMv4OBRFxg+6/oGvvLaag609zBtczl1yx6nwOtrfMf9pOBNrxLKZy/n8xfdSdxngcD7BqudwvI81W/+WPb27WD71LKp5G+IMnjq2cQCdPCqDYinIoj4e3/0E393yfdw0IfU5A0kdoxHihWAUITCWMAquXM2ZBNZ3ruNHnf9GtMjx1ZVf4x06lzQZ5LN8ie/s/B47+/ZS4FEExJbPj/JsBM8NPM3fbHwQklaqqUKljmXolIg9Gc2JlvI0FwSnEYuqi1n4S0tAA2iGeghS4fGd/8ShA0e5et51XL3kQ+UuQBDsCD8KgYCXoiTfG5x61h99lfue/wpz2mex8h3X8MjLf4+zUBBQKRDNQCtjClCvppwQm/Fq10bqyRFmtc1jnptKNJiQuYLDroc8Sjl3wZlUJMaocCwwl7rIo4UhGI8Xj/Mxe+tv8Wcv3ktL0c5Hzvlt/u7ZB2jFoFoezWE4rIKKEMRgx0F002EIgsGUcsaAsQYrgkggGEGMY0e6nS9v/CozWjv4g4s+SZtpRSMFA5YEMbZkWUu2AwFPIBDYWezgM8/fTT0M8Ol338JSN4PCBqwavASwICKlYhvD8UDAqpIgtCZVrI/o3LWXr7/8NfrNUZ4+9DxPda3lAreM2864jSoRThxiDCJgASEHUQSDBKFXurl73T28NrCH2867jeVTzuJg2xH648Cg2GGhEgjDspZxlopOLWGROjYX+vwA97/y52x3u/jjc+5kaWURxphROlMBFcWoIdIEBQ65o3z2+QfY0t/Jn575Rd5fuxYbqqTOkhpLLVikcAxRIStFcFNXIo4yGHsySbjh9KtYzlJ644gvb/8Gv/nMpdzy3Md5T/wbfOt9DzI96mhqw6ojMznqIfOe1dvu5z96HudTcz7KTQuuYHYWE0SIvDC1KHMJq5ZAhBcHCPYXQrTGaDA8tu8JHut6kqtmXc6li9+LiMGY0tQJZAuoFOWkF6U6+M6Wh3l2z6PcPPfDXH7WStTmRKIoKcZ4kMGGYwarIGMMRLIqUlhSVYKB05YsRrynPzZsybcTZfvpmFWlnkSobx6NQ+Gw6lE5ytquJ/nW5h9w2dQPcuMFHyPSNuo4Bm1C3VbIrFBIjgYPqhSAqjAi5t8eYx+GTeCJ2Z5t5osv/gXTwlzuWPYp4iIhxKHclidnTgpBPBZL0MBPutbz7c0Ps7BtER8+7bfY3r+NToTuwUFq9RbqST8b+17iV6rLaNcORG2ZiTbxpR5VqGY5uwZf4eZ1n+Zgz24+d/bvQdHOmv/5LoejTh7Z/gQy0MJ9K+8Cpo02IgHxhm0D27h3w+exrTWuOft69g/s4DBddGZv0DEINZfzSt82shZlYTQdG1rwIoSfovV/ZqLrYYgvbFrNAX+Q22ffwdwpixEV7FhsaJn8GFGCKpv2b6Kr1s3ufIDL111Dro6qN6jNSCuGzqEhbnzsdr5w8T1cvfgqvIkRae6kGs++aBefW/8ZXq+/zO+uuImbz7iVIDEXzj+fP9xwN9vrb/LjN59iy9k3cF77aKLF9lP4Fl7o3Ud3upuoHrj9qT8iN334MIUgikl62a193PjkXdy59Pf5+DuvI8oSjI3xCG6cMWHMWkdopKlGLVpAPcr4l73/zA/3Pcvs9HSu/9UriYyCWkTG6E0CTmMEwbuM05JFfEhWMdRS0NN6hLbBmFoWeNXuYOfQPtpoZfnMdzEzmY00jk4zRnQTlB+/tZn1h15mBo7fOfNaTN5KXvO8q30lt5xzK59YfwcHqr30pj1NbQQMuU2ZHk/j3e2rSKOUgUpO1dchJOzXg2zt30NNElZMWcLS6mJUqvgIHAraPJkaN9GlChJEy5O2cJ7dxS4e3rCGNBri3BkXMK9lIZE6vGnMTJMeVRQTLKoGg2HVsg9y2VkfoBoqDIhSCxYjKd987Xvcu+mvmDN9Jqsvu4e5zMKoJdbGyd6kmOMKz96DO+irBOJgCVoQA8lQlcL0czTbSdUXrMiWcG77GU0Hb30LNfWsmvNrXHLF+QSyUvIRYUILz3St45ZnP8psM4e/XrWahZXZCIIYGa6XjSP/eRuiKWO8a5xDdVLWdf03nfU3iGK4aPH5uFBFjaGsLtGcaMpJQyEmxqrFa06khnYcNrfkTlGTk9sCU1hmaI3YxxQOnA+UonW0bVGYO2M25vWEgajOQxse4mNn3kTkZrPl8Aa+vvkRxFS57p03MdXMaTr4zAgBh8ERa4wJNUSV1A4SF46KOvrjXtqljSoV4uHKZJnCH0t6xqOj3yZGa6MeIGhQXt/5OrW0Ri2tsqz1dKIQ4a0yfOU4RiVTKXkSFWxwGIHc1gHBxJY6nkKVJK/SMjSdKFTxAnWgYmTsJNzGrJx/CTfM38TanWv54ZtP8197f4K1Fu1TZrbO5+YLr2fVgivGHKGIL0u9CIUYxAiKwQePiiISqPqYSIRSCpRFsoAps8xTgDS7nC3LxAHFI2rJtWBrz1YOpm+BJpzXcTYzzDT6IyEOnlgMmNGxtCCUW01Lt0pJLBBKJ7WAIMrudA9v9G0nca2c13EOsbEUohhsWZ1r4nheBNQofXqEjYde5MXuzeyv72NmBEvbV3BRx3uZG8/BBkUcYJoUuXwO6uFYoclYVATJFS2EntDHS90bmRLXWDFtBUmcEEbcL+UnYzg4HqLLhMdTmBQbHFK4sjZsPbkIlSAYEXoN1LySiAU7ujdPOWHgS6JpVAQ1oGQIFglRqUpChmqMdxYnGSYowVRpJKejXUz7UFMhiKUIHg0e9QXOCGlisRJRzS0YxdsCJ6NLtnUNBC23tUFAyuoKISWVHBVLzVcQAt4MEtOGl5Ez0ITG4hlH7Bib6GM3JMORQRofj2t+wkVK8xg9dhI90khO/HqcaRmuS4x6TEdajvyvTba0NmyM5V9T4yOdHkfDMAfHSqzHO/tT0JzoSfzcMXk5O0GYJHqCMEn0BGGS6AnCJNEThEmiJwiTRE8QJomeIEwSPUGYJHqCMEn0BOF/AZGzd6M1DSORAAAAAElFTkSuQmCC";
            result = "{\"orderNo\":\"" + orderNo + "\",\"route\":\"" + route + "\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\"" + new Date().toLocaleString() + "\",\"data\":{\"status\":200,\"validatedCode\":\"" + validatedCode + "\"}}";
        } else {
            result = "{\"orderNo\":\"" + orderNo + "\",\"route\":\"" + route + "\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\"" + new Date().toLocaleString() + "\",\"data\":{\"status\":200,\"recordNo\":\"失信结果\"}}";
        }
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", result));
        paramsList.add(new BasicNameValuePair("route", route));

        LOG.info("爬虫程序返回验证码【法院执行信息（个人、企业）】 orderNo=" + orderNo + ", result=" + result);
        return super.doPostWithParams(url, paramsList);
    }

    /**
     * 第二次拿验证码
     *
     * @return
     */
    @RequestMapping(value = "/webcrawler/findValidatedCode2/court", method = RequestMethod.POST)
    @ResponseBody
    public String findValidatedCode2ForCrawler() {
        String json = null;
        String url = super.properties.get("url.court.findValidatedCode2").toString();
        String httpResult = super.doPost(url);
        //如果是JSON
        if (httpResult.startsWith("{")) {
            json = httpResult;
        } else {
            json = "{\"status\":201,\"errorMsg\":\"" + httpResult + "\"}";
        }
        LOG.info("爬虫程序轮询行内获取验证码请求【法院执行信息（个人、企业）】 httpResult=" + json);
        return json;
    }

    /**
     * 第二次返回验证码
     */
    @RequestMapping(value = "/webcrawler/returnValidatedCode2/court", method = RequestMethod.POST)
    @ResponseBody
    public String returnValidatedCode2ForCrawler(
            @RequestParam final String orderNo) {
        String url = super.properties.get("url.court.returnValidatedCode2").toString();
        String validatedCode = "iVBORw0KGgoAAAANSUhEUgAAAFoAAAAkCAYAAAAJgC2zAAAKiklEQVR4nO2Ya4xd1XXHf2vvc859zZ2nxwa/mPgB42BwY+rEk4gQ4oSQNhEpr7YUqS0ufIiEkjbi4biBRFQtdkBJRAU0UZQoaZOQBEVJqEkhbmlcg3k6EMAUCMb4ocHjmfE87uucs/fqh3MNHzwO90bVfLp/6Ur3wz3r7vXb6/z3XktUVWlVDlRSGkax3mBSUHUggEr2MQYJIBWIEESkpdCKA1VIQW1CIoJqHisNVBIMAdblACWxdULyCKa12B5QB1LDmYiUiFAVC9Dy+pQYT4BicAiCqgGxCO8cQ9oBraooDi8OEDyQkmJJcBhiIgRLESX0DiTXciIpMU4VqxEgGBSjCWBQEU7kIghePEJrCQI4wDfXKRrisRiJmxsVtpY7kKAE+tZSSCVbT9DC86385i0l4kGFMI5Q7xEF40NEcwQGIiuIFUQ8StoqYwCMDxBVRONmBpY08uAF6yxiwJkEBIwLEfM2/FPpRA0ZBcHixWMwGA/eeERafScAhdADAh5FBUQFQVsqprZAW28Qn1XHaHCIfe5/GZ2YZiauk3ewZmCIVYUVdJtBAvIZjFbziD1TyTRP2d0cHD+MrZTpKg2yetFyVtjldLkCIkoqDkPAO1FWVVQ1s67Uk/gah80xIgp0UyYgR2iEVpcoqmjsaeRqjPoxQg0ZtAsI0wii/2/QCOpgNmrw+Qf/noemd5BzJWIRfNAg54T3FNex9fzb+IPyeU0grWkqPM71O67nYXbT1TVIsVpkovoGA7bM9vffwUULL8JgURK8NP31d0hE3oJdD+p8/4X7+MoT95JvWLaMfI5L1l2Kbb2eASG1jt9UXmTLz7ZSqOe556q7WWqWtfR0W6CdNNBQMD7HcO483rN2hHOL59AVlXhk+jH++YV7edS9wLb/up17/+RuBjitZR+1WuQvhv+G7Wdsp18X4NRx/9T3uWnPFu5//sdc+JEPkvN5MCY7dzm5phXFK1gnqE1x3pFY5Qcv38+25+7i0OJDdNcsfcsKWJ9k1W5Phu1UsaR4sYgHxOO94TfxK2x98J94KdzPmoGzSPIW4tbYtQVaJMTjCcXwtx+6HhEH3hGQY8WSszhw7HV+cvS7vFp5hTfTKgNR67G7bMRHz/wgsamQUqNY6+H8RR/BuNuJiXG2CuSxFLEeZI5iVIXUO7xLsCgO4Qe//Qm3PfuPLOtdSGOqh1JYYGlpGFWLWA9zVLXzCqaOEuDTAKN1Xmu8yc27b6C8oJ/BydNY07uanHZDAuTeOb923h1ElQAI1RMKGFGwDlWlpEJZEyqhEpdK9JrurOxalOIR6wkxBGrwgePVqefpq5QYWTJCqEXwklWt0TlDZxWeUo3qpD7Hg/v/k68+eScbFq3niuHLqQUwEPUwGHahGPwp3rYwFRwFlAbeBBxP6tyy92YGK33csOE6Rs0EqwaGGHR54mKjpfzaAu1FcSiiAZIGaCPExRFT1Pj52EM8MLqTUmy59t1XsbDS17JtAFgfQhzgG3l8nOfl+DXufuzrfGDJRi4Z/hSBy8pG8KTC3GeheCJ1dLlefjq+gxv3fpqzBs/gjo3bSCZT6iZlqPtdFDTCqeBPlb514A2p66Imk9z47B1UJsa49eKbSKYCTFJjTc9qTGpwpjXQ7VmHZkdQbB3TdoqvP/0v7K08zcGZ/RyYnWZ5eRVb3n0zly67GHUx2sZdt2FifvT6D9k1tYvJ2gxH3hzl8nP/nMuG/5ge7cMnggWMKu4UIcULaQKPHN3FjU9+mvf1nMvtH/gy/WEf+8cPEJCwuncNVgqonnplHkdVHKU4ZNtTd7JvYjf3bPoa78ovZWf9F/TFOVbmzyCNIKf2Ha+Z0K51NBO1HoxXZmemGJ8ZI40d+SRmdvYwTx55jtfqYyTWtRMarym1eBqrQlpNOGoPs+fQfzMzOYliSQPAOEQhVOa0JSeOhyZ28pkn/o513e9l+/l3sUSGmPEJL6YvkU9qnNm/lrrNE9hTJ+/UEvsZ7nrpq/zqyG6+PHIra3JrEc2zr/JrSj2n0x/1YsQhWmyNXTudofcpoiAuAFUSkzArdabjSfYee5TtT27juWg/I/YCvnPhNzgtv/CkFlwBT7Zh0rw7eBHUK54YUUvdKd8bu49/ePwL/GFuI9/82D0UtAcCR96lxCbCiGle8RTVzE72V9/g0ocvZ8KNc8WKK1maPx0UGknMvS9/g9ge409XbObC/ov4+JIPU5Bgzrt+teH42Us/Zuu+L3LdyF9zcc/H8E6oBQ3u3PMljknIbSM3cHYwRF+4GNOCL7QF2mlCogYvlkA9BoeTAKM1UMO//vaHfO75m8inIV9bfweXr7riJOvImniwzb/NeitBnSLNzlMbhqlojL/8j7/imekDPHPZDvp0OfWco5w2qJo81hpCzQ5oR0pDlJ2v7mLzrzcTaRUwJORQSRF1eBsCMb21MptXb+YzGz5L4RRN1RvuMNc+cA3P6l6KM92UkxKJqeOJqJUOEdYGUdfD3Zu2cuHSTxC1QLotj1bJUBlVEI+gBB6ECE2FVeUzKVd78EHM69Ovo+hJoEUhwKNiULILsXgPxlMxhjpCt/OkVBk3VQajhQSSQyRzfMRgBCxNaxRAMxtYXl7CF9d9niyKxfmAxMb8z9iveOTwLs4Ohrli3WV8eNEFFHx4ys65J+3mlo23UtEKxaSEAElY55n4eW7Z+wW2rLyMDYs/yTm9yzAkLWFsrzNUwaaKaMKsHsdEhpAC1ueZCCZ5eOLfme4ao7fWxxk9Q7/jIFRQcNllDUwF8UKu0UWgEEd1Hj60i8PT+7lk6M/I2TyqJ0DbtxtwyTbOiCHAM7xwJWv7zwTjwAmoIQkTDo4fQc2jrO1bz5XnXE2/7wbMnIUA0GWKbFzwPgSDJAJWSW3KkVePU6oNcNHCTazvG8lmKCnQQr/Q3lAJgws8Da1y/TdvpmtNP6t7VtBv+3jm6B5+MfoAJhLOLa5j07JNc7ZvJwwjm8V4xtNx7tvzHVYOrWRV7zDGO375ym6+9dy/MVRYxTVnX01oCoAjIAAJsNIcGIlkw53mLM8CWEfDpIQSYL3juExzcOIQpXqJ4d5hilrAqiW1ioE5W/k4qGEwBBg0JziUhIR9E09TsiELFi0D61Ea1CWg0ALptkAbFcQZYltnqjDOrtHH+eVBmA2mybuIRe40/mjBlVx73jWU6Zozxon22ToB8RyrHmXHgZ0cPPhtCENSDSmmCRcsfi/XrP8sZ0WrQQUvCVYtYBD1J03MDICCVyUztWzOMZtU8VXHUDzIcM9Scs25eWZ/c+eZp4tsEuxxJsYSgBOOH59kxG6gbBfhcARO0KC19retwxAPqOJNzJTMcDh5k+rsLLO+QjnXzeL8YvqCBYQ+qwYROQmIknV1xgsqSp06k0wx1hhlujJDEOU5vdjP6XaAnCsDttkJZiNOaZqyQjP2W+9I9lUF37QUUUhMSk1qKCkF8oQaIWpRyUDPZR1v74HHS7NNV6hIBYslrwWkeX1S29oEsD3QHf3eaqth6ej3Vwf0PKkDep7UAT1P6oCeJ3VAz5M6oOdJHdDzpA7oeVIH9DypA3qe1AE9T+qAnid1QM+TOqDnSR3Q86QO6HnS/wGNU7ptwt5UbAAAAABJRU5ErkJggg==";
        String result = "{\"orderNo\":\"" + orderNo + "\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\"" + new Date().toLocaleString() + "\",\"data\":{\"status\":200,\"validatedCode\":\"" + validatedCode + "\"}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", result));

        LOG.info("爬虫程序返回验证码【法院执行信息（个人、企业）】 orderNo=" + orderNo + ", result=" + result);
        return super.doPostWithParams(url, paramsList);
    }

    /**
     * 获取查询结果
     */
    @RequestMapping(value = "/webcrawler/findResultWithValidatedCode/court", method = RequestMethod.POST)
    @ResponseBody
    public String findResultForCrawler(@RequestParam final String orderNo) {
        String url = super.properties.get("url.court.findResult").toString();
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        LOG.info("爬虫程序轮询获取行内查询结果请求【法院执行信息（个人、企业）】 orderNo=" + orderNo);
        return super.doPostWithParams(url, paramsList);
    }

    /**
     * 返回结果
     */
    @RequestMapping(value = "/webcrawler/returnResultWithValidatedCode/court", method = RequestMethod.POST)
    @ResponseBody
    public String returnResultForCrawler(@RequestParam final String orderNo,
                                         @RequestParam final String msg) {
        String url = super.properties.get("url.court.returnResult").toString();
        String jsonStr = "{\"orderNo\":\"" + orderNo + "\",\"status\":\"200\",\"message\":\"query success.\",\"date\":\"" + new Date().toLocaleString()
                + "\",\"data\":{\"branchOrganization\":[],\"investorInfo\":[{\"investor\":\"" + msg + "\"}]}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", jsonStr));

        LOG.info("爬虫程序返回结果【法院执行信息（个人、企业）】 resultStr=" + jsonStr);
        return super.doPostWithParams(url, paramsList);
    }

    @RequestMapping("/court/test")
    public String index() {
        return "courtCrawler";
    }
}
