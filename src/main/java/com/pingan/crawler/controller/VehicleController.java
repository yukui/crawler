package com.pingan.crawler.controller;

import com.pingan.crawler.controller.base.BaseController;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 平安行内查询：违章
 * Created by yukui on 2016/12/20.
 */
@Controller
public class VehicleController extends BaseController {

    public static final Logger LOG = LoggerFactory.getLogger(VehicleController.class);

    /**
     * 违章轮询
     * @return
     */
    @RequestMapping(value = "/webcrawler/vehicle/findValidatedCode/peccancy", method = RequestMethod.POST)
    @ResponseBody
    public String findValidatedCodeForPeccancy() {
        String json = null;
        String url = super.properties.get("url.vehicle.findValidatedCode.peccancy").toString();
        String httpResult = super.doPost(url);
        //如果是JSON
        if(httpResult.startsWith("{")){
            json = httpResult;
        }else{
            json = "{\"status\":201,\"errorMsg\":\""+httpResult+"\"}";
        }
        LOG.info("爬虫程序轮询行内获取验证码请求【违章】 httpResult=" + json);
        return json;
    }

    /**
     * 违章返回验证码
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnValidated/peccancy", method = RequestMethod.POST)
    @ResponseBody
    public String returnValidatedCodeForPeccancy(
            @RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.returnValidated.peccancy").toString();
        String validatedCode = "iVBORw0KGgoAAAANSUhEUgAAAFoAAAAkCAYAAAAJgC2zAAALxElEQVR4nO2ZaZBdxXXHf6e7773vvZnRaBntK3ICAUmExZig2NgOBgsIxkCKEH+AiokrhcHEcYCkUsQ4jh0wia2KF8CWTZyUiV0hdooQonIcCDGhhIxAAgmkgEAaLYO20Yw027tL98mH+zQjad7gkcuefMj8P7y17+nT/+4+/T+nRVWVSfzCYf6vHfj/gkmiJwiTRE8QJomeILimv+qxNwUCCEgwBEANiAqiICgq2mgZUDGIGgxSGhFovIyY1oZ5UUzjSyGACk5L+yUERBufTrRxso+lPQFVjHq8WLwoBo8BRC2CGe73uB5G+VcaLcA71BZ4CViNAUWkSdtxoumKViAoQCBogYaCkAfUKz5AQMEDBXhVQpGjZOQhK4lUxZPTXM4oqp6MgFeP5h4NQuYVDUoeMoIqBPDl9DW14lUJxzkcfPmbBo/3SpEqPvOEvCAv8pNG11hAY9hOQz+agWYpPgxSeCWEvGnb8aLpig4SKKQgUof4iFxyBqM+nEIihkEKcGBIyInBQlVj4sIhQHBKKgVVolG2hYBhCNGEQizOKoQhvB3isAQq1LA4EBo7ozlSE3BqiCgXqdGMQgr2mJSQepym1GxE1bZj1BEIyAk7TFCarmcKY8EplgQVT92mxAximX6K9I6gKdGCYrQMEd4GNh7YxEMbHiS3Q4ga6i7DBUiKCp4aqQmsmvd+PrLsWiqaIKIY4qYdKoLXhDg4cuPZxwEeeu4BdvV1sqCygDvf9ydURYBQhiikKRtxCOVEqDBAnaPmCGu3/ivf73yUXT3dHJY+plcrXDrn17n13E+w0P0ykYJgAIPKsbGORiWrsT/u5R9f+DYb979AWzyPu9/zSeZUfjaSYSyi1eDUgEJmU7qHDrBh/3p6WvopbPlIJfdY9aRJoN/mLEo7yOyVJFhEhNhHYEaT5DHkYjBFIDNHWPPSGr6x7xGyaIALh87H4pAgqCkA2zw+A86X78FCt+nj/v/8Ej/a+e8s6JjPladfwhHfzVO7nuHRN5+gyGL+8uL7MFjcWMv4OBRFxg+6/oGvvLaag609zBtczl1yx6nwOtrfMf9pOBNrxLKZy/n8xfdSdxngcD7BqudwvI81W/+WPb27WD71LKp5G+IMnjq2cQCdPCqDYinIoj4e3/0E393yfdw0IfU5A0kdoxHihWAUITCWMAquXM2ZBNZ3ruNHnf9GtMjx1ZVf4x06lzQZ5LN8ie/s/B47+/ZS4FEExJbPj/JsBM8NPM3fbHwQklaqqUKljmXolIg9Gc2JlvI0FwSnEYuqi1n4S0tAA2iGeghS4fGd/8ShA0e5et51XL3kQ+UuQBDsCD8KgYCXoiTfG5x61h99lfue/wpz2mex8h3X8MjLf4+zUBBQKRDNQCtjClCvppwQm/Fq10bqyRFmtc1jnptKNJiQuYLDroc8Sjl3wZlUJMaocCwwl7rIo4UhGI8Xj/Mxe+tv8Wcv3ktL0c5Hzvlt/u7ZB2jFoFoezWE4rIKKEMRgx0F002EIgsGUcsaAsQYrgkggGEGMY0e6nS9v/CozWjv4g4s+SZtpRSMFA5YEMbZkWUu2AwFPIBDYWezgM8/fTT0M8Ol338JSN4PCBqwavASwICKlYhvD8UDAqpIgtCZVrI/o3LWXr7/8NfrNUZ4+9DxPda3lAreM2864jSoRThxiDCJgASEHUQSDBKFXurl73T28NrCH2867jeVTzuJg2xH648Cg2GGhEgjDspZxlopOLWGROjYX+vwA97/y52x3u/jjc+5kaWURxphROlMBFcWoIdIEBQ65o3z2+QfY0t/Jn575Rd5fuxYbqqTOkhpLLVikcAxRIStFcFNXIo4yGHsySbjh9KtYzlJ644gvb/8Gv/nMpdzy3Md5T/wbfOt9DzI96mhqw6ojMznqIfOe1dvu5z96HudTcz7KTQuuYHYWE0SIvDC1KHMJq5ZAhBcHCPYXQrTGaDA8tu8JHut6kqtmXc6li9+LiMGY0tQJZAuoFOWkF6U6+M6Wh3l2z6PcPPfDXH7WStTmRKIoKcZ4kMGGYwarIGMMRLIqUlhSVYKB05YsRrynPzZsybcTZfvpmFWlnkSobx6NQ+Gw6lE5ytquJ/nW5h9w2dQPcuMFHyPSNuo4Bm1C3VbIrFBIjgYPqhSAqjAi5t8eYx+GTeCJ2Z5t5osv/gXTwlzuWPYp4iIhxKHclidnTgpBPBZL0MBPutbz7c0Ps7BtER8+7bfY3r+NToTuwUFq9RbqST8b+17iV6rLaNcORG2ZiTbxpR5VqGY5uwZf4eZ1n+Zgz24+d/bvQdHOmv/5LoejTh7Z/gQy0MJ9K+8Cpo02IgHxhm0D27h3w+exrTWuOft69g/s4DBddGZv0DEINZfzSt82shZlYTQdG1rwIoSfovV/ZqLrYYgvbFrNAX+Q22ffwdwpixEV7FhsaJn8GFGCKpv2b6Kr1s3ufIDL111Dro6qN6jNSCuGzqEhbnzsdr5w8T1cvfgqvIkRae6kGs++aBefW/8ZXq+/zO+uuImbz7iVIDEXzj+fP9xwN9vrb/LjN59iy9k3cF77aKLF9lP4Fl7o3Ud3upuoHrj9qT8iN334MIUgikl62a193PjkXdy59Pf5+DuvI8oSjI3xCG6cMWHMWkdopKlGLVpAPcr4l73/zA/3Pcvs9HSu/9UriYyCWkTG6E0CTmMEwbuM05JFfEhWMdRS0NN6hLbBmFoWeNXuYOfQPtpoZfnMdzEzmY00jk4zRnQTlB+/tZn1h15mBo7fOfNaTN5KXvO8q30lt5xzK59YfwcHqr30pj1NbQQMuU2ZHk/j3e2rSKOUgUpO1dchJOzXg2zt30NNElZMWcLS6mJUqvgIHAraPJkaN9GlChJEy5O2cJ7dxS4e3rCGNBri3BkXMK9lIZE6vGnMTJMeVRQTLKoGg2HVsg9y2VkfoBoqDIhSCxYjKd987Xvcu+mvmDN9Jqsvu4e5zMKoJdbGyd6kmOMKz96DO+irBOJgCVoQA8lQlcL0czTbSdUXrMiWcG77GU0Hb30LNfWsmvNrXHLF+QSyUvIRYUILz3St45ZnP8psM4e/XrWahZXZCIIYGa6XjSP/eRuiKWO8a5xDdVLWdf03nfU3iGK4aPH5uFBFjaGsLtGcaMpJQyEmxqrFa06khnYcNrfkTlGTk9sCU1hmaI3YxxQOnA+UonW0bVGYO2M25vWEgajOQxse4mNn3kTkZrPl8Aa+vvkRxFS57p03MdXMaTr4zAgBh8ERa4wJNUSV1A4SF46KOvrjXtqljSoV4uHKZJnCH0t6xqOj3yZGa6MeIGhQXt/5OrW0Ri2tsqz1dKIQ4a0yfOU4RiVTKXkSFWxwGIHc1gHBxJY6nkKVJK/SMjSdKFTxAnWgYmTsJNzGrJx/CTfM38TanWv54ZtP8197f4K1Fu1TZrbO5+YLr2fVgivGHKGIL0u9CIUYxAiKwQePiiISqPqYSIRSCpRFsoAps8xTgDS7nC3LxAHFI2rJtWBrz1YOpm+BJpzXcTYzzDT6IyEOnlgMmNGxtCCUW01Lt0pJLBBKJ7WAIMrudA9v9G0nca2c13EOsbEUohhsWZ1r4nheBNQofXqEjYde5MXuzeyv72NmBEvbV3BRx3uZG8/BBkUcYJoUuXwO6uFYoclYVATJFS2EntDHS90bmRLXWDFtBUmcEEbcL+UnYzg4HqLLhMdTmBQbHFK4sjZsPbkIlSAYEXoN1LySiAU7ujdPOWHgS6JpVAQ1oGQIFglRqUpChmqMdxYnGSYowVRpJKejXUz7UFMhiKUIHg0e9QXOCGlisRJRzS0YxdsCJ6NLtnUNBC23tUFAyuoKISWVHBVLzVcQAt4MEtOGl5Ez0ITG4hlH7Bib6GM3JMORQRofj2t+wkVK8xg9dhI90khO/HqcaRmuS4x6TEdajvyvTba0NmyM5V9T4yOdHkfDMAfHSqzHO/tT0JzoSfzcMXk5O0GYJHqCMEn0BGGS6AnCJNEThEmiJwiTRE8QJomeIEwSPUGYJHqCMEn0BOF/AZGzd6M1DSORAAAAAElFTkSuQmCC";
        String result = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\""+new Date().toLocaleString()+"\",\"data\":{\"status\":200,\"validatedCode\":\""+validatedCode+"\"}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", result));

        LOG.info("爬虫程序返回验证码【违章】 orderNo=" + orderNo+", result="+result);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 违章获取查询结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/findResult/peccancy", method = RequestMethod.POST)
    @ResponseBody
    public String findResultForPeccancy(@RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.findResult.peccancy").toString();
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        LOG.info("爬虫程序轮询获取行内查询结果请求【违章】 orderNo=" + orderNo);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 违章返回结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnResult/peccancy", method = RequestMethod.POST)
    @ResponseBody
    public String returnResultForPeccancy(@RequestParam final String orderNo,
                                         @RequestParam final String msg) {
        String url = super.properties.get("url.vehicle.returnResult.peccancy").toString();
        String jsonStr = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"query success.\",\"date\":\""+new Date().toLocaleString()
                +"\",\"data\":{\"branchOrganization\":[],\"investorInfo\":[{\"investor\":\""+msg+"\"}]}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", jsonStr));

        LOG.info("爬虫程序返回结果【违章】 resultStr=" + jsonStr);
        return super.doPostWithParams(url,paramsList);
    }

    //---------------------------计分 ---------------------------------------------
    /**
     * 计分轮询
     * @return
     */
    @RequestMapping(value = "/webcrawler/vehicle/findValidatedCode/scoring", method = RequestMethod.POST)
    @ResponseBody
    public String findValidatedCodeForScoring() {
        String json = null;
        String url = super.properties.get("url.vehicle.findValidatedCode.scoring").toString();
        String httpResult = super.doPost(url);
        //如果是JSON
        if(httpResult.startsWith("{")){
            json = httpResult;
        }else{
            json = "{\"status\":201,\"errorMsg\":\""+httpResult+"\"}";
        }
        LOG.info("爬虫程序轮询行内获取验证码请求【计分】 httpResult=" + json);
        return json;
    }

    /**
     * 计分返回验证码
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnValidated/scoring", method = RequestMethod.POST)
    @ResponseBody
    public String returnValidatedCodeForScoring(
            @RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.returnValidated.scoring").toString();
        String validatedCode = "iVBORw0KGgoAAAANSUhEUgAAAFoAAAAkCAYAAAAJgC2zAAALxElEQVR4nO2ZaZBdxXXHf6e7773vvZnRaBntK3ICAUmExZig2NgOBgsIxkCKEH+AiokrhcHEcYCkUsQ4jh0wia2KF8CWTZyUiV0hdooQonIcCDGhhIxAAgmkgEAaLYO20Yw027tL98mH+zQjad7gkcuefMj8P7y17+nT/+4+/T+nRVWVSfzCYf6vHfj/gkmiJwiTRE8QJomeILimv+qxNwUCCEgwBEANiAqiICgq2mgZUDGIGgxSGhFovIyY1oZ5UUzjSyGACk5L+yUERBufTrRxso+lPQFVjHq8WLwoBo8BRC2CGe73uB5G+VcaLcA71BZ4CViNAUWkSdtxoumKViAoQCBogYaCkAfUKz5AQMEDBXhVQpGjZOQhK4lUxZPTXM4oqp6MgFeP5h4NQuYVDUoeMoIqBPDl9DW14lUJxzkcfPmbBo/3SpEqPvOEvCAv8pNG11hAY9hOQz+agWYpPgxSeCWEvGnb8aLpig4SKKQgUof4iFxyBqM+nEIihkEKcGBIyInBQlVj4sIhQHBKKgVVolG2hYBhCNGEQizOKoQhvB3isAQq1LA4EBo7ozlSE3BqiCgXqdGMQgr2mJSQepym1GxE1bZj1BEIyAk7TFCarmcKY8EplgQVT92mxAximX6K9I6gKdGCYrQMEd4GNh7YxEMbHiS3Q4ga6i7DBUiKCp4aqQmsmvd+PrLsWiqaIKIY4qYdKoLXhDg4cuPZxwEeeu4BdvV1sqCygDvf9ydURYBQhiikKRtxCOVEqDBAnaPmCGu3/ivf73yUXT3dHJY+plcrXDrn17n13E+w0P0ykYJgAIPKsbGORiWrsT/u5R9f+DYb979AWzyPu9/zSeZUfjaSYSyi1eDUgEJmU7qHDrBh/3p6WvopbPlIJfdY9aRJoN/mLEo7yOyVJFhEhNhHYEaT5DHkYjBFIDNHWPPSGr6x7xGyaIALh87H4pAgqCkA2zw+A86X78FCt+nj/v/8Ej/a+e8s6JjPladfwhHfzVO7nuHRN5+gyGL+8uL7MFjcWMv4OBRFxg+6/oGvvLaag609zBtczl1yx6nwOtrfMf9pOBNrxLKZy/n8xfdSdxngcD7BqudwvI81W/+WPb27WD71LKp5G+IMnjq2cQCdPCqDYinIoj4e3/0E393yfdw0IfU5A0kdoxHihWAUITCWMAquXM2ZBNZ3ruNHnf9GtMjx1ZVf4x06lzQZ5LN8ie/s/B47+/ZS4FEExJbPj/JsBM8NPM3fbHwQklaqqUKljmXolIg9Gc2JlvI0FwSnEYuqi1n4S0tAA2iGeghS4fGd/8ShA0e5et51XL3kQ+UuQBDsCD8KgYCXoiTfG5x61h99lfue/wpz2mex8h3X8MjLf4+zUBBQKRDNQCtjClCvppwQm/Fq10bqyRFmtc1jnptKNJiQuYLDroc8Sjl3wZlUJMaocCwwl7rIo4UhGI8Xj/Mxe+tv8Wcv3ktL0c5Hzvlt/u7ZB2jFoFoezWE4rIKKEMRgx0F002EIgsGUcsaAsQYrgkggGEGMY0e6nS9v/CozWjv4g4s+SZtpRSMFA5YEMbZkWUu2AwFPIBDYWezgM8/fTT0M8Ol338JSN4PCBqwavASwICKlYhvD8UDAqpIgtCZVrI/o3LWXr7/8NfrNUZ4+9DxPda3lAreM2864jSoRThxiDCJgASEHUQSDBKFXurl73T28NrCH2867jeVTzuJg2xH648Cg2GGhEgjDspZxlopOLWGROjYX+vwA97/y52x3u/jjc+5kaWURxphROlMBFcWoIdIEBQ65o3z2+QfY0t/Jn575Rd5fuxYbqqTOkhpLLVikcAxRIStFcFNXIo4yGHsySbjh9KtYzlJ644gvb/8Gv/nMpdzy3Md5T/wbfOt9DzI96mhqw6ojMznqIfOe1dvu5z96HudTcz7KTQuuYHYWE0SIvDC1KHMJq5ZAhBcHCPYXQrTGaDA8tu8JHut6kqtmXc6li9+LiMGY0tQJZAuoFOWkF6U6+M6Wh3l2z6PcPPfDXH7WStTmRKIoKcZ4kMGGYwarIGMMRLIqUlhSVYKB05YsRrynPzZsybcTZfvpmFWlnkSobx6NQ+Gw6lE5ytquJ/nW5h9w2dQPcuMFHyPSNuo4Bm1C3VbIrFBIjgYPqhSAqjAi5t8eYx+GTeCJ2Z5t5osv/gXTwlzuWPYp4iIhxKHclidnTgpBPBZL0MBPutbz7c0Ps7BtER8+7bfY3r+NToTuwUFq9RbqST8b+17iV6rLaNcORG2ZiTbxpR5VqGY5uwZf4eZ1n+Zgz24+d/bvQdHOmv/5LoejTh7Z/gQy0MJ9K+8Cpo02IgHxhm0D27h3w+exrTWuOft69g/s4DBddGZv0DEINZfzSt82shZlYTQdG1rwIoSfovV/ZqLrYYgvbFrNAX+Q22ffwdwpixEV7FhsaJn8GFGCKpv2b6Kr1s3ufIDL111Dro6qN6jNSCuGzqEhbnzsdr5w8T1cvfgqvIkRae6kGs++aBefW/8ZXq+/zO+uuImbz7iVIDEXzj+fP9xwN9vrb/LjN59iy9k3cF77aKLF9lP4Fl7o3Ud3upuoHrj9qT8iN334MIUgikl62a193PjkXdy59Pf5+DuvI8oSjI3xCG6cMWHMWkdopKlGLVpAPcr4l73/zA/3Pcvs9HSu/9UriYyCWkTG6E0CTmMEwbuM05JFfEhWMdRS0NN6hLbBmFoWeNXuYOfQPtpoZfnMdzEzmY00jk4zRnQTlB+/tZn1h15mBo7fOfNaTN5KXvO8q30lt5xzK59YfwcHqr30pj1NbQQMuU2ZHk/j3e2rSKOUgUpO1dchJOzXg2zt30NNElZMWcLS6mJUqvgIHAraPJkaN9GlChJEy5O2cJ7dxS4e3rCGNBri3BkXMK9lIZE6vGnMTJMeVRQTLKoGg2HVsg9y2VkfoBoqDIhSCxYjKd987Xvcu+mvmDN9Jqsvu4e5zMKoJdbGyd6kmOMKz96DO+irBOJgCVoQA8lQlcL0czTbSdUXrMiWcG77GU0Hb30LNfWsmvNrXHLF+QSyUvIRYUILz3St45ZnP8psM4e/XrWahZXZCIIYGa6XjSP/eRuiKWO8a5xDdVLWdf03nfU3iGK4aPH5uFBFjaGsLtGcaMpJQyEmxqrFa06khnYcNrfkTlGTk9sCU1hmaI3YxxQOnA+UonW0bVGYO2M25vWEgajOQxse4mNn3kTkZrPl8Aa+vvkRxFS57p03MdXMaTr4zAgBh8ERa4wJNUSV1A4SF46KOvrjXtqljSoV4uHKZJnCH0t6xqOj3yZGa6MeIGhQXt/5OrW0Ri2tsqz1dKIQ4a0yfOU4RiVTKXkSFWxwGIHc1gHBxJY6nkKVJK/SMjSdKFTxAnWgYmTsJNzGrJx/CTfM38TanWv54ZtP8197f4K1Fu1TZrbO5+YLr2fVgivGHKGIL0u9CIUYxAiKwQePiiISqPqYSIRSCpRFsoAps8xTgDS7nC3LxAHFI2rJtWBrz1YOpm+BJpzXcTYzzDT6IyEOnlgMmNGxtCCUW01Lt0pJLBBKJ7WAIMrudA9v9G0nca2c13EOsbEUohhsWZ1r4nheBNQofXqEjYde5MXuzeyv72NmBEvbV3BRx3uZG8/BBkUcYJoUuXwO6uFYoclYVATJFS2EntDHS90bmRLXWDFtBUmcEEbcL+UnYzg4HqLLhMdTmBQbHFK4sjZsPbkIlSAYEXoN1LySiAU7ujdPOWHgS6JpVAQ1oGQIFglRqUpChmqMdxYnGSYowVRpJKejXUz7UFMhiKUIHg0e9QXOCGlisRJRzS0YxdsCJ6NLtnUNBC23tUFAyuoKISWVHBVLzVcQAt4MEtOGl5Ez0ITG4hlH7Bib6GM3JMORQRofj2t+wkVK8xg9dhI90khO/HqcaRmuS4x6TEdajvyvTba0NmyM5V9T4yOdHkfDMAfHSqzHO/tT0JzoSfzcMXk5O0GYJHqCMEn0BGGS6AnCJNEThEmiJwiTRE8QJomeIEwSPUGYJHqCMEn0BOF/AZGzd6M1DSORAAAAAElFTkSuQmCC";
        String result = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\""+new Date().toLocaleString()+"\",\"data\":{\"status\":200,\"validatedCode\":\""+validatedCode+"\"}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", result));

        LOG.info("爬虫程序返回验证码【计分】 orderNo=" + orderNo+", result="+result);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 计分获取查询结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/findResult/scoring", method = RequestMethod.POST)
    @ResponseBody
    public String findResultForScoring(@RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.findResult.scoring").toString();
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        LOG.info("爬虫程序轮询获取行内查询结果请求【计分】 orderNo=" + orderNo);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 计分返回结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnResult/scoring", method = RequestMethod.POST)
    @ResponseBody
    public String returnResultForScoring(@RequestParam final String orderNo,
                                         @RequestParam final String msg) {
        String url = super.properties.get("url.vehicle.returnResult.scoring").toString();
        String jsonStr = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"query success.\",\"date\":\""+new Date().toLocaleString()
                +"\",\"data\":{\"branchOrganization\":[],\"investorInfo\":[{\"investor\":\""+msg+"\"}]}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", jsonStr));

        LOG.info("爬虫程序返回结果【计分】 resultStr=" + jsonStr);
        return super.doPostWithParams(url,paramsList);
    }

//---------------------------报废 ---------------------------------------------
    /**
     * 报废轮询
     * @return
     */
    @RequestMapping(value = "/webcrawler/vehicle/findValidatedCode/scrap", method = RequestMethod.POST)
    @ResponseBody
    public String findValidatedCodeForScrap() {
        String json = null;
        String url = super.properties.get("url.vehicle.findValidatedCode.scrap").toString();
        String httpResult = super.doPost(url);
        //如果是JSON
        if(httpResult.startsWith("{")){
            json = httpResult;
        }else{
            json = "{\"status\":201,\"errorMsg\":\""+httpResult+"\"}";
        }
        LOG.info("爬虫程序轮询行内获取验证码请求【报废】 httpResult=" + json);
        return json;
    }

    /**
     * 报废返回验证码
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnValidated/scrap", method = RequestMethod.POST)
    @ResponseBody
    public String returnValidatedCodeForScrap(
            @RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.returnValidated.scrap").toString();
        String validatedCode = "iVBORw0KGgoAAAANSUhEUgAAAFoAAAAkCAYAAAAJgC2zAAALxElEQVR4nO2ZaZBdxXXHf6e7773vvZnRaBntK3ICAUmExZig2NgOBgsIxkCKEH+AiokrhcHEcYCkUsQ4jh0wia2KF8CWTZyUiV0hdooQonIcCDGhhIxAAgmkgEAaLYO20Yw027tL98mH+zQjad7gkcuefMj8P7y17+nT/+4+/T+nRVWVSfzCYf6vHfj/gkmiJwiTRE8QJomeILimv+qxNwUCCEgwBEANiAqiICgq2mgZUDGIGgxSGhFovIyY1oZ5UUzjSyGACk5L+yUERBufTrRxso+lPQFVjHq8WLwoBo8BRC2CGe73uB5G+VcaLcA71BZ4CViNAUWkSdtxoumKViAoQCBogYaCkAfUKz5AQMEDBXhVQpGjZOQhK4lUxZPTXM4oqp6MgFeP5h4NQuYVDUoeMoIqBPDl9DW14lUJxzkcfPmbBo/3SpEqPvOEvCAv8pNG11hAY9hOQz+agWYpPgxSeCWEvGnb8aLpig4SKKQgUof4iFxyBqM+nEIihkEKcGBIyInBQlVj4sIhQHBKKgVVolG2hYBhCNGEQizOKoQhvB3isAQq1LA4EBo7ozlSE3BqiCgXqdGMQgr2mJSQepym1GxE1bZj1BEIyAk7TFCarmcKY8EplgQVT92mxAximX6K9I6gKdGCYrQMEd4GNh7YxEMbHiS3Q4ga6i7DBUiKCp4aqQmsmvd+PrLsWiqaIKIY4qYdKoLXhDg4cuPZxwEeeu4BdvV1sqCygDvf9ydURYBQhiikKRtxCOVEqDBAnaPmCGu3/ivf73yUXT3dHJY+plcrXDrn17n13E+w0P0ykYJgAIPKsbGORiWrsT/u5R9f+DYb979AWzyPu9/zSeZUfjaSYSyi1eDUgEJmU7qHDrBh/3p6WvopbPlIJfdY9aRJoN/mLEo7yOyVJFhEhNhHYEaT5DHkYjBFIDNHWPPSGr6x7xGyaIALh87H4pAgqCkA2zw+A86X78FCt+nj/v/8Ej/a+e8s6JjPladfwhHfzVO7nuHRN5+gyGL+8uL7MFjcWMv4OBRFxg+6/oGvvLaag609zBtczl1yx6nwOtrfMf9pOBNrxLKZy/n8xfdSdxngcD7BqudwvI81W/+WPb27WD71LKp5G+IMnjq2cQCdPCqDYinIoj4e3/0E393yfdw0IfU5A0kdoxHihWAUITCWMAquXM2ZBNZ3ruNHnf9GtMjx1ZVf4x06lzQZ5LN8ie/s/B47+/ZS4FEExJbPj/JsBM8NPM3fbHwQklaqqUKljmXolIg9Gc2JlvI0FwSnEYuqi1n4S0tAA2iGeghS4fGd/8ShA0e5et51XL3kQ+UuQBDsCD8KgYCXoiTfG5x61h99lfue/wpz2mex8h3X8MjLf4+zUBBQKRDNQCtjClCvppwQm/Fq10bqyRFmtc1jnptKNJiQuYLDroc8Sjl3wZlUJMaocCwwl7rIo4UhGI8Xj/Mxe+tv8Wcv3ktL0c5Hzvlt/u7ZB2jFoFoezWE4rIKKEMRgx0F002EIgsGUcsaAsQYrgkggGEGMY0e6nS9v/CozWjv4g4s+SZtpRSMFA5YEMbZkWUu2AwFPIBDYWezgM8/fTT0M8Ol338JSN4PCBqwavASwICKlYhvD8UDAqpIgtCZVrI/o3LWXr7/8NfrNUZ4+9DxPda3lAreM2864jSoRThxiDCJgASEHUQSDBKFXurl73T28NrCH2867jeVTzuJg2xH648Cg2GGhEgjDspZxlopOLWGROjYX+vwA97/y52x3u/jjc+5kaWURxphROlMBFcWoIdIEBQ65o3z2+QfY0t/Jn575Rd5fuxYbqqTOkhpLLVikcAxRIStFcFNXIo4yGHsySbjh9KtYzlJ644gvb/8Gv/nMpdzy3Md5T/wbfOt9DzI96mhqw6ojMznqIfOe1dvu5z96HudTcz7KTQuuYHYWE0SIvDC1KHMJq5ZAhBcHCPYXQrTGaDA8tu8JHut6kqtmXc6li9+LiMGY0tQJZAuoFOWkF6U6+M6Wh3l2z6PcPPfDXH7WStTmRKIoKcZ4kMGGYwarIGMMRLIqUlhSVYKB05YsRrynPzZsybcTZfvpmFWlnkSobx6NQ+Gw6lE5ytquJ/nW5h9w2dQPcuMFHyPSNuo4Bm1C3VbIrFBIjgYPqhSAqjAi5t8eYx+GTeCJ2Z5t5osv/gXTwlzuWPYp4iIhxKHclidnTgpBPBZL0MBPutbz7c0Ps7BtER8+7bfY3r+NToTuwUFq9RbqST8b+17iV6rLaNcORG2ZiTbxpR5VqGY5uwZf4eZ1n+Zgz24+d/bvQdHOmv/5LoejTh7Z/gQy0MJ9K+8Cpo02IgHxhm0D27h3w+exrTWuOft69g/s4DBddGZv0DEINZfzSt82shZlYTQdG1rwIoSfovV/ZqLrYYgvbFrNAX+Q22ffwdwpixEV7FhsaJn8GFGCKpv2b6Kr1s3ufIDL111Dro6qN6jNSCuGzqEhbnzsdr5w8T1cvfgqvIkRae6kGs++aBefW/8ZXq+/zO+uuImbz7iVIDEXzj+fP9xwN9vrb/LjN59iy9k3cF77aKLF9lP4Fl7o3Ud3upuoHrj9qT8iN334MIUgikl62a193PjkXdy59Pf5+DuvI8oSjI3xCG6cMWHMWkdopKlGLVpAPcr4l73/zA/3Pcvs9HSu/9UriYyCWkTG6E0CTmMEwbuM05JFfEhWMdRS0NN6hLbBmFoWeNXuYOfQPtpoZfnMdzEzmY00jk4zRnQTlB+/tZn1h15mBo7fOfNaTN5KXvO8q30lt5xzK59YfwcHqr30pj1NbQQMuU2ZHk/j3e2rSKOUgUpO1dchJOzXg2zt30NNElZMWcLS6mJUqvgIHAraPJkaN9GlChJEy5O2cJ7dxS4e3rCGNBri3BkXMK9lIZE6vGnMTJMeVRQTLKoGg2HVsg9y2VkfoBoqDIhSCxYjKd987Xvcu+mvmDN9Jqsvu4e5zMKoJdbGyd6kmOMKz96DO+irBOJgCVoQA8lQlcL0czTbSdUXrMiWcG77GU0Hb30LNfWsmvNrXHLF+QSyUvIRYUILz3St45ZnP8psM4e/XrWahZXZCIIYGa6XjSP/eRuiKWO8a5xDdVLWdf03nfU3iGK4aPH5uFBFjaGsLtGcaMpJQyEmxqrFa06khnYcNrfkTlGTk9sCU1hmaI3YxxQOnA+UonW0bVGYO2M25vWEgajOQxse4mNn3kTkZrPl8Aa+vvkRxFS57p03MdXMaTr4zAgBh8ERa4wJNUSV1A4SF46KOvrjXtqljSoV4uHKZJnCH0t6xqOj3yZGa6MeIGhQXt/5OrW0Ri2tsqz1dKIQ4a0yfOU4RiVTKXkSFWxwGIHc1gHBxJY6nkKVJK/SMjSdKFTxAnWgYmTsJNzGrJx/CTfM38TanWv54ZtP8197f4K1Fu1TZrbO5+YLr2fVgivGHKGIL0u9CIUYxAiKwQePiiISqPqYSIRSCpRFsoAps8xTgDS7nC3LxAHFI2rJtWBrz1YOpm+BJpzXcTYzzDT6IyEOnlgMmNGxtCCUW01Lt0pJLBBKJ7WAIMrudA9v9G0nca2c13EOsbEUohhsWZ1r4nheBNQofXqEjYde5MXuzeyv72NmBEvbV3BRx3uZG8/BBkUcYJoUuXwO6uFYoclYVATJFS2EntDHS90bmRLXWDFtBUmcEEbcL+UnYzg4HqLLhMdTmBQbHFK4sjZsPbkIlSAYEXoN1LySiAU7ujdPOWHgS6JpVAQ1oGQIFglRqUpChmqMdxYnGSYowVRpJKejXUz7UFMhiKUIHg0e9QXOCGlisRJRzS0YxdsCJ6NLtnUNBC23tUFAyuoKISWVHBVLzVcQAt4MEtOGl5Ez0ITG4hlH7Bib6GM3JMORQRofj2t+wkVK8xg9dhI90khO/HqcaRmuS4x6TEdajvyvTba0NmyM5V9T4yOdHkfDMAfHSqzHO/tT0JzoSfzcMXk5O0GYJHqCMEn0BGGS6AnCJNEThEmiJwiTRE8QJomeIEwSPUGYJHqCMEn0BOF/AZGzd6M1DSORAAAAAElFTkSuQmCC";
        String result = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"get verify code success.\",\"date\":\""+new Date().toLocaleString()+"\",\"data\":{\"status\":200,\"validatedCode\":\""+validatedCode+"\"}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", result));

        LOG.info("爬虫程序返回验证码【报废】 orderNo=" + orderNo+", result="+result);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 报废获取查询结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/findResult/scrap", method = RequestMethod.POST)
    @ResponseBody
    public String findResultForScrap(@RequestParam final String orderNo) {
        String url = super.properties.get("url.vehicle.findResult.scrap").toString();
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        LOG.info("爬虫程序轮询获取行内查询结果请求【报废】 orderNo=" + orderNo);
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 报废返回结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnResult/scrap", method = RequestMethod.POST)
    @ResponseBody
    public String returnResultForScrap(@RequestParam final String orderNo,
                                         @RequestParam final String msg) {
        String url = super.properties.get("url.vehicle.returnResult.scrap").toString();
        String jsonStr = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"query success.\",\"date\":\""+new Date().toLocaleString()
                +"\",\"data\":{\"branchOrganization\":[],\"investorInfo\":[{\"investor\":\""+msg+"\"}]}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", jsonStr));

        LOG.info("爬虫程序返回结果【报废】 resultStr=" + jsonStr);
        return super.doPostWithParams(url,paramsList);
    }

    //------------------------------车价---------------------------------
    /**
     * 车价获取查询结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/findResult/price", method = RequestMethod.POST)
    @ResponseBody
    public String findResultForPrice() {
        String url = super.properties.get("url.vehicle.findResult.price").toString();
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        LOG.info("爬虫程序轮询获取行内查询结果请求【车价】");
        return super.doPostWithParams(url,paramsList);
    }

    /**
     * 车价返回结果
     */
    @RequestMapping(value = "/webcrawler/vehicle/returnResult/price", method = RequestMethod.POST)
    @ResponseBody
    public String returnResultForPrice(@RequestParam final String orderNo,
                                       @RequestParam final String msg) {
        String url = super.properties.get("url.vehicle.returnResult.price").toString();
        String jsonStr = "{\"orderNo\":\""+orderNo+"\",\"status\":\"200\",\"message\":\"query success.\",\"date\":\""+new Date().toLocaleString()
                +"\",\"data\":{\"branchOrganization\":[],\"investorInfo\":[{\"investor\":\""+msg+"\"}]}}";
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
        paramsList.add(new BasicNameValuePair("result", jsonStr));

        LOG.info("爬虫程序返回结果【车价】 resultStr=" + jsonStr);
        return super.doPostWithParams(url,paramsList);
    }

}
