package com.pingan.crawler.controller.base;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class BaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(BaseController.class);
	
	public static ConcurrentMap<String, Object> orderNoMap = new ConcurrentHashMap<String, Object>();
	
	
	// 线程池为无限大，当执行第二个任务时如果第一个任务已经完成，会复用执行第一个任务的线程，而不用每次新建线程。
	protected static ExecutorService executorService = Executors
			.newCachedThreadPool();

	@Resource(name = "configProperties")
	protected Properties properties;
	private static int requestTimeOutSeconds;

	/**
	 * 行外地址IP:PORT/content
	 */
	public static String domain;
	
	@PostConstruct
	public void init() {
		domain = properties.getProperty("url.domain").toString();
		LOG.info("对象初始化完成：domain=" + domain);
		requestTimeOutSeconds = Integer.valueOf(properties.getProperty(
				"request.timeout.seconds").toString());
		LOG.info("对象初始化完成：requestTimeOutSeconds=" + requestTimeOutSeconds);
	}
	
	public String doPostWithParams(final String urlStr,
			final List<NameValuePair> paramsList) {
		
		final String logInfo = domain+urlStr + ",参数="
				+ JSONObject.toJSONString(paramsList);
		LOG.info("爬虫抓取请求：" + logInfo);

		String result = null;

		// 采用线程超时机制，以免造成应用程序长时间线程堵塞
		Callable<String> callable = new Callable<String>() {
			public String call() throws Exception {
				String resultStr = null;
				CloseableHttpClient httpclient = HttpClients.createDefault();
				// 创建httppost
				HttpPost httpPost = new HttpPost(domain + urlStr);
				CloseableHttpResponse response = null;
				try {
					UrlEncodedFormEntity uefEntity = new UrlEncodedFormEntity(
							paramsList, "UTF-8");
					httpPost.setEntity(uefEntity);
					// 执行http请求
					response = httpclient.execute(httpPost);
					HttpEntity entity = response.getEntity();
					if (entity != null) {
						resultStr = EntityUtils.toString(entity, "UTF-8");
					}
				} catch (ClientProtocolException t) {
					LOG.warn("客户端协议异常："+logInfo, t);
				} catch (UnsupportedEncodingException t) {
					LOG.warn("请求异常："+logInfo, t);
				} catch (IOException t) {
					LOG.warn("请求异常："+logInfo, t);
				} finally {
					// 关闭连接,释放资源
					try {
						if(response != null){
							response.close();
						}
					} catch (IOException t) {
						LOG.error("response流关闭异常。"+logInfo, t);
					}
					try {
						if(httpclient != null){
							httpclient.close();
						}
					} catch (IOException t) {
						LOG.error("httpclient流关闭异常。"+logInfo, t);
					}
				}
				return resultStr;
			}
		};

		try {
			// 使用一个线程执行者来启动一个设定了超时时间的线程
			Future<String> future = executorService.submit(callable);
			// 设置超时时间，若规定时间内无返回内容，中断线程执行
			result = future.get(requestTimeOutSeconds, TimeUnit.SECONDS);
		} catch (InterruptedException e) { // 主线程在等待返回结果时被中断！
			LOG.warn("主线程在等待返回结果时被中断！logInfo=" + logInfo, e);
		} catch (ExecutionException e) { // 主线程等待返回结果，但执行返回结果代码抛出异常！
			LOG.warn("主线程等待返回结果，但执行返回结果代码抛出异常！logInfo=" + logInfo, e);
		} catch (TimeoutException e) { // 主线程等待返回结果超时，因此中断任务线程！
			LOG.warn("主线程等待返回结果超时，因此中断任务线程！logInfo=" + logInfo, e);
		} catch (RejectedExecutionException e) {
			LOG.warn("executorService.submit一个线程发生Rejected异常！logInfo="
					+ logInfo, e);
		}
		LOG.info("爬虫抓取请求：" + logInfo+",result="+result);
		return result;
	}
	

	public String doPost(final String urlStr) {
		
		final String logInfo = domain+urlStr;

		String result = null;

		// 采用线程超时机制，以免造成应用程序长时间线程堵塞
		Callable<String> callable = new Callable<String>() {
			public String call() throws Exception {
				String resultStr = null;
				CloseableHttpClient httpclient = HttpClients.createDefault();
				// 创建httppost
				HttpPost httpPost = new HttpPost(domain + urlStr);
				CloseableHttpResponse response = null;
				try {
					// 执行http请求
					response = httpclient.execute(httpPost);
					HttpEntity entity = response.getEntity();
					if (entity != null) {
						resultStr = EntityUtils.toString(entity, "UTF-8");
					}
				} catch (ClientProtocolException t) {
					LOG.warn("客户端协议异常："+logInfo, t);
				} catch (UnsupportedEncodingException t) {
					LOG.warn("请求异常："+logInfo, t);
				} catch (IOException t) {
					LOG.warn("请求异常："+logInfo, t);
				} finally {
					// 关闭连接,释放资源
					try {
						if(response != null){
							response.close();
						}
					} catch (IOException t) {
						LOG.error("response流关闭异常。"+logInfo, t);
					}
					try {
						if(httpclient != null){
							httpclient.close();
						}
					} catch (IOException t) {
						LOG.error("httpclient流关闭异常。"+logInfo, t);
					}
				}
				return resultStr;
			}
		};

		try {
			// 使用一个线程执行者来启动一个设定了超时时间的线程
			Future<String> future = executorService.submit(callable);
			// 设置超时时间，若规定时间内无返回内容，中断线程执行
			result = future.get(requestTimeOutSeconds, TimeUnit.SECONDS);
		} catch (InterruptedException e) { // 主线程在等待返回结果时被中断！
			LOG.warn("主线程在等待返回结果时被中断！logInfo=" + logInfo, e);
		} catch (ExecutionException e) { // 主线程等待返回结果，但执行返回结果代码抛出异常！
			LOG.warn("主线程等待返回结果，但执行返回结果代码抛出异常！logInfo=" + logInfo, e);
		} catch (TimeoutException e) { // 主线程等待返回结果超时，因此中断任务线程！
			LOG.warn("主线程等待返回结果超时，因此中断任务线程！logInfo=" + logInfo, e);
		} catch (RejectedExecutionException e) {
			LOG.warn("executorService.submit一个线程发生Rejected异常！logInfo="
					+ logInfo, e);
		}
		return result;
	}
}
