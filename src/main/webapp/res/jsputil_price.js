var domain = $("#domain").html();
var contextPath = $("#ContextPath").html();




var orderNoParam = "";



var toFindResult = function(form){
	var resultDiv = form.find("#showResult3");
	var orderNoVal = $("input[name='orderNo']").val();

	//显示提示信息
	resultDiv.html(new Date().Format("HH:mm:ss.S") +"，等待返回轮询结果...");
	$.ajax({
		url : contextPath+"/webcrawler/vehicle/findResult/price.do",
		method : "POST",
		dataType : "json",
		success : function(data) {
			//打印返回内容
			console.log("result data==", data);
			if(data && data.data && data.data.orderNo){
				orderNoParam = data.data.orderNo;
			}
			$("input[name='orderNo']").val(orderNoParam);
			//返回错误信息
			var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data);
			resultDiv.html(resultContent);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
}

var toReturnResult = function(form){
	var resultDiv = form.find("#showResult4");
	//显示提示信息
	resultDiv.html(new Date().Format("HH:mm:ss.S") +"，等待返回轮询结果...");
	var param = {};
	param["orderNo"] = $("input[name='orderNo']").val();;
	param["msg"] = $("input[name='message']").val();

	console.log(param);
	$.ajax({
		url : contextPath+"/webcrawler/vehicle/returnResult/price.do",
		method : "POST",
		dataType : "json",
		data: param,
		success : function(data) {
			//打印返回内容
			console.log("result data==", data);
			//返回错误信息
			var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data);
			resultDiv.html(resultContent);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
	
}

