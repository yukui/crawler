/**
 * @ZhuWen 对jQuery对象做获取form表单全部值的扩展方法values()，使用该js类必须先导入jQuery库
 * 
准备dom节点(input输入框,多选框,单选框,下拉框):
<form id="myform">
  用户名：<input type="text" name="username"></input><br/>  
	性别: 
  <input type="radio" name="sex" value="男">男</input>
  <input type="radio" name="sex" value="女">女</input>
  <br/>  
  兴趣：
  <input type="checkbox" name="interests" value="旅游">旅游</input>
  <input type="checkbox" name="interests" value="美食">美食</input>
  <input type="checkbox" name="interests" value="唱歌">唱歌</input>
  <br/>  
  国家：
  <select name="country">
	  <option value ="中国">中国</option>
	  <option value ="美国">美国</option>
	  <option value="英国">英国</option>
	  <option value="法国">法国</option>
	</select>
	<br/>  
	<input type="button" value="提交" onclick="submitThisForm()">
</form>

使用方法：
function submitThisForm(){
	var values = $("#myform").values();
	console.log("values=",values);
	//打印：values= {username: "Zhang", interests: Array[3], sex: "男", country: "中国"}
}
 */
jQuery.fn.extend({
	values:function(){
		var values = {};
		var form = $(this);
		if(form && form.get(0) && form.get(0).tagName === "FORM"){
			//扫描input输入框
			var inputs = form.find("input[type=text]");
			$.each(inputs,function(index,node){
				var identifying = node.name || node.id;
				if(identifying){
					values[identifying] = $(node).val();
				}
			});
			//扫描多选框
			var checkboxs = form.find("input[type=checkbox]");
			$.each(checkboxs,function(index,node){
				var checkboxNode = $(node).get(0);
				if(checkboxNode.checked){
					var identifying = node.name;
					if(identifying){
						var arr = values[identifying];
						if(!arr){
							arr = [];
						}
						arr.push($(node).val());
						values[identifying] = arr;
					}
				}
			});
			//扫描单选框
			var radios = form.find("input[type=radio]:checked");
			$.each(radios,function(index,node){
				var radioNode = $(node).get(0);
				if(radioNode.checked){
					var identifying = node.name;
					if(identifying){
						values[identifying] = $(node).val();
					}
				}
			});
			//扫描下拉框
			var selects = form.find("select");
			$.each(selects,function(index,node){
				var identifying = node.name;
				if(identifying){
					values[identifying] = $(node).val();
				}
			});
		}
		return values;
	}
});