<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.pingan.crawler.controller.base.BaseController" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: ClientController.java url: /controller/test  -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>爬虫端|车价</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
form{border: #003333 solid 1px;}
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>

 <div>
 测试行外项目server地址：
 <div id="domain"><%=BaseController.domain %></div>
 <div id="ContextPath" style="display: none;"><%=request.getContextPath()%></div>
 <br/>
 
 </div>


<form id="resultForm" method="post">
<p>爬虫轮询行内获取结果【车价】，返回车价结果</p>
	<table>

		<tr>
			<td >
				<input type="button" onclick='toFindResult($(this.form))' value="轮询一下" />
				<font id="showResult3"></font>
			</td>
		</tr>
		<tr>
			<td>orderNo:<input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td >
			         输入车价信息:<input type="text" name="message" value="车价信息"/>
			    <br/>
				<input type="button" onclick='toReturnResult($(this.form))' value="返回结果" />
				<font id="showResult4"></font>
			</td>
		</tr>
	</table>
</form>

</br>



</body>
<script src="<%=request.getContextPath()%>/res/jsputil_price.js"></script>
</html>